package netsecfinalserver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * @author Max Kiniklis
 */

public class BlockChain {
       
    public PrivateKey privKey;
    public PublicKey pubKey;
    private List<Block> chain = new ArrayList<>();
    
    // Constructor
    public BlockChain( PrivateKey privKey, PublicKey pubKey ) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException, FileNotFoundException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, ParseException, UnsupportedEncodingException, IOException
    {
        this.privKey = privKey;
        this.pubKey = pubKey;
        
//        newGenBlock( "Genesis Block", "Genesis Block", "NEW", "Server", this.privKey );

        loadChain();
    }
    
    
    /********* Standard Running Functions *********/
    
    // Create a new Block Object
    public void newBlock( String data, String docName, String transType, String editor, PrivateKey prKey ) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException
    {
        int index = this.chain.size();
        String prevHash = chainHeadHash();

        Block block = new Block();
        block.newBlock( index, data, docName, transType, editor, prevHash, prKey);
        
        
        if( validBlock( block ) )
        {
            addToChain( block );
        }
        
    }
    
    public void newGenBlock( String data, String docName, String transType, String editor, PrivateKey prKey ) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException
    {
        int index = 0;
        Block block = new Block();
        block.newBlock( index, data, docName, transType, editor, "N/A", prKey);
        
        
        if( validBlock( block ) )
        {
            addToChain( block );
        }
        
    }
    
    
    // Append a newly created Block to this block chain
    private boolean validBlock( Block b)
    {
        boolean valid = true;
        
        // Validation loop
        Block current = b;
        for (int i = chain.size() - 1; i >= 0; i--) {
            Block block = chain.get(i);
            if (block.getCurrHash().equals(current.getPrevHash())) {
                current = block;
                
            } else {
                valid = false;
                throw new RuntimeException("Block Invalid");
            }
        }
        return valid;
    }
    
    
    
    // Return the head of the block chains hash so it can be used to create a new block
    private String chainHeadHash()
    {
        return getHead().getCurrHash();
    }
    
    
    // Get the head of the block chain
    public Block getHead() {

        Block result = null;
        if (this.chain.size() > 0) {
            result = this.chain.get(this.chain.size() - 1);
        } else {

            throw new RuntimeException("No Block's have been added to chain...");
        }

        return result;
    }
    
    
    
    
    /********* Save Block Chain Functions *********/
     /*
     * This function condenses all of the data in each block and the chain 
     * and sends it to the Block Chain Writer class to store it. 
     */
    public void saveChain() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, IOException
    {
        // New Block Chain Writer Object
        BCWriter save = new BCWriter();
        
        // Condense all of the block data into one string
        String chainData = "";
        
        // Loop through chain and return the data for each block
        for( int i = 0; i < this.chain.size(); i++)     
        {
            chainData += chain.get(i).condenseBlock();
        }
        
        // Save all of the block data
        save.saveChain( chainData );
        
    }
    
    // Add a block to the chain and save the chain.
    private void addToChain( Block b ) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, IOException
    {
        this.chain.add( b );
        saveChain();
    }
    
    
    /********* Load Block Chain Functions *********/
    
    // Todo: Parse data and call "newBlock()"
    public void loadChain() throws NoSuchAlgorithmException, InvalidKeySpecException, FileNotFoundException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, SignatureException, ParseException, IOException
    {
        BCReader load = new BCReader( this.privKey );
        
        String data = load.readFile();
        
        String[] blockString = data.split(";"); //Each array space contains the data of a single block
        
        //Loop through each block and build the chain
        for( String block : blockString )
        {
            String[] blockData = block.split("\\|");
            int index = Integer.parseInt( blockData[0] );
            
            loadBC( index, blockData[1], blockData[2], blockData[3], blockData[4], blockData[5], blockData[6], blockData[7], blockData[8] );
        }
        
    }
    
    
    // Create a new Block Object
    public void loadBC( int index, String timestamp, String currHash, String prevHash, String data, String docName, String transType, String lastEditor, String signature ) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException, ParseException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException
    {

        Block block = new Block();
        block.loadBlock(index, timestamp, currHash, prevHash, data, docName, transType, lastEditor, signature );
        
        boolean validBlock = block.verifySignature( this.pubKey );

        if( validBlock )
        {
            this.chain.add( block );
        } else {
            System.out.println("Invalid Signature for block " + index );
        }
        
    }
    
    
    
    
    
    /******** Print Block Chain to File for Client ********/
    public void createBCFile() throws FileNotFoundException, UnsupportedEncodingException {
        File bcFile = new File( "./BlockChainContents.txt");
        PrintWriter writer = new PrintWriter( bcFile );
        
        //Get a timestamp for the file
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        String timestamp = ts.toString();
        
        writer.println("Contents of the Block Chain as of " + timestamp + "\n\n");
        
        int indexCtr = 0;
        // For each block in the chain, write contents to the file
        // Loop through chain and return the data for each block
        for( int i = 0; i < this.chain.size(); i++)     
        {
            writer.println("-------- Block " + indexCtr + " --------");
            writer.println("Document Name: " + chain.get(i).docName );
            writer.println("User: " + chain.get(i).lastEditor );
            writer.println("Timestamp: " + chain.get(i).timestamp );
            writer.println("Transaction Type: " + chain.get(i).transactionType );
            writer.println("Data: " + chain.get(i).data);
            writer.println("Previous Hash: " + chain.get(i).prevHash);
            writer.println("Current Hash: " + chain.get(i).currHash);
            writer.println("Block Signature: " + chain.get(i).signature);
            writer.println("-------- End of Block " + indexCtr + " --------\n\n\n\n");
            
            indexCtr++;
        }
        writer.close();
        
    }
    
    
    
    /**** Create Block from file ****/
    public void blockFromFile( String user, String fName, List<String> dataList, String transType ) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException
    {
        //Convert the List<String> into one String
        String data = "";
        for (String line : dataList) {
            data += line + "\n";
        }
        
        System.out.println( data );
        
        newBlock( data, fName, transType, user, privKey );
    }
    
    
}
