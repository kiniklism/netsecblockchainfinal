package netsecfinalserver;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 *
 * @author Max Kiniklis
 */
public class Account {
    private String dbName = "accounts.db";
    private File dbFile = new File( dbName );
    private FlatDatabase flatDB = new FlatDatabase();
    private String[] dbFields = {"Username", "Salt", "Password"};
    private byte[] salt; 
    private String uName, pWord;
    final int NUM_ITERS = 10000;
    private String ack;
    
            
    /*
        Constructor
        
        Take in and parse string to get the username and password
        Start the DB
        Check the command for either CREATE or AUTH
        Call respective functions
    */
    public Account( String msg ) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        String[] msgContents = msg.split(":");
        
        String operation = msgContents[0];
        this.uName = msgContents[1];
        this.pWord = msgContents[2];
        
        //Start DB
        runDB();
        
        switch( operation )
        {
            case "CREATE":
                createAcc();
                break;
            case "AUTH":
                authAcc();
                break;
            default:
                this.ack = "NOK";
                break;
        }
        
        
    }
    
    // Checks if DB exists and creates a DB if it does not. 
    // If it already exists, open the DB
    // I could be wrong but calling FlatDatabase.openDatabase() always 
    // returns true whether the file exists or not
    private void runDB()
    {
        if( dbFile.exists() )
        {
            // Open DB
            boolean dbOpened = flatDB.openDatabase( dbName );
            if( !dbOpened )
                System.out.println("Error opening '" + dbName + "'");
        }
        else
        {
            // Create DB
            boolean dbCreated = flatDB.createDatabase( dbName, dbFields );
            if( !dbCreated )
                System.out.println("Error creating '" + dbName + "'"); 
        }
    }
    
    
    private void genSalt()
    {
        byte[] rawIV = new byte[16];
        SecureRandom rand = new SecureRandom();
        rand.nextBytes(rawIV);          // Fill array with random bytes.
        this.salt = rawIV;
    }
    
    
    private void createAcc() throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        genSalt();
        byte[] hashedPass = hashPassword( this.pWord );
        
        String b64Salt = Base64.getEncoder().encodeToString( this.salt );
        String b64Hash = Base64.getEncoder().encodeToString( hashedPass );
        
        String[] fieldVal = {this.uName, b64Salt, b64Hash};
        
        // Create new Record
        Record r = new Record( this.dbFields, fieldVal );
        
        // Add to DB
        boolean recAdded = flatDB.insertRecord( r );
        
        if( !recAdded )
        {
            System.out.println("Error adding record.");
            this.ack = "NOK";
        }
        
        dbSave();
        this.ack = "OK";
    }
    

    private void authAcc()
    {
        String db64Salt, db64Hash;
        byte[] dbSalt, dbHash;
        
        Record r;
        try
        {
            r = flatDB.lookupRecord("Username", this.uName );
            
            db64Salt = r.getFieldValue("Salt");
            db64Hash = r.getFieldValue("Password");

            dbSalt = Base64.getDecoder().decode( db64Salt );
            dbHash = Base64.getDecoder().decode( db64Hash );
            
            
            this.salt = dbSalt;
            
            byte[] hashedPass = hashPassword( this.pWord );
            
            boolean passMatch = Arrays.equals(dbHash, hashedPass);
            
            if( passMatch )
            {
                this.ack = "OK";
            }
            else
            {
                this.ack = "NOK";
            }
            
        }
        catch(Exception AEADBadTagException)
        {
            System.out.println("\nNo results found. Make sure your information is correct.\n\n\n\n");
            this.ack = "NOK";
        }

    }
    
    
    
    private byte[] hashPassword( String password ) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        KeySpec spec = new PBEKeySpec(password.toCharArray(), this.salt, NUM_ITERS, 128);
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
        byte[] hashedPass = factory.generateSecret(spec).getEncoded();
        
        return hashedPass;
    }
    
    
    
    
    // Save DB
    private void dbSave()
    {
        boolean saved = flatDB.saveDatabase();
        
        if( !saved )
            System.out.println("Error saving database.");
            
    }
    
    public String getAck()
    {
        return this.ack;
    }
    
    public String getuName() {
        return uName;
    }
    
    
}
