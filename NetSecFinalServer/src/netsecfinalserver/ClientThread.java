package netsecfinalserver;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import static java.lang.System.in;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author Max Kiniklis
 */
public class ClientThread extends Thread {
    private Socket sock;
    private BlockChain blockChain;
    private String userName;
    private Scanner recv;
    private PrintWriter send;
    private boolean userValid = false;
    
    public ClientThread( Socket socket, BlockChain bc ) throws IOException
    {
        this.sock = socket;
        this.blockChain = bc;
        this.send = new PrintWriter(sock.getOutputStream(), true);
        this.recv = new Scanner(sock.getInputStream());
        
        
    }
    
    public void run() {
        boolean stopMenu = false;
        
        try {
            
            //Setup the streams for use.
            Scanner recv = new Scanner(sock.getInputStream());
            PrintWriter send = new PrintWriter(sock.getOutputStream(), true);
            
            
            /**** Authentication ****/
            String clientMsg = recv.nextLine();      // Get line from Client
            Account user = new Account( clientMsg );
            
            
            // If not authenticated
            if( "NOK".equals( user.getAck() ))
            {   
                send.println( "NOK" );
                System.out.println( "Error - Authentication Failed. Closing Connection." );
                // Close the connection.
                sock.close();
            } 
            else
            {
                System.out.println( "User '" + user.getuName() + "' Authenticated" );
                this.userName = user.getuName();
                send.println( "OK" );
                userValid = true;
            }
            
            
            /**** Client Menu Selection ****/
            if( userValid )
            {
                String menuSelect = recv.nextLine();
                System.out.println(menuSelect);
                
                // Parse menu select and read command
                String[] parseCommand = menuSelect.split(":");
                
                switch( parseCommand[0] ) {
                    case "VIEW":
                        blockChain.createBCFile();
                        File blockChainHistory = new File("BlockChainContents.txt");
                        sendFile( blockChainHistory );
                        break;
                    case "EDIT":
                        String fileSelection = getFileNames();
                        send.println( fileSelection );
                        String nameOFile = recv.nextLine();
                        File requestedFile = new File("BCFiles/" + nameOFile);
                        sendFile( requestedFile );
                        break;
                    case "NEW":
                        String fName = parseCommand[1];
                        send.println( "OK" );
                        recvFile( fName );
                        List<String> data = fileToString( fName );
                
                    try {
                        blockChain.blockFromFile( userName, fName, data, parseCommand[0] );
                    } catch (InvalidKeyException ex) {
                        Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (SignatureException ex) {
                        Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NoSuchPaddingException ex) {
                        Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalBlockSizeException ex) {
                        Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (BadPaddingException ex) {
                        Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
                    }
                
                        break;
                    case "STOP":
                        stopMenu = true;
                        break; 
                    default:
                        break;
                }
             
            }
            
            
            // Close the connection.
            sock.close();
            

            
        } catch (IOException ex) {
            Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
    }
    

    
    private void sendFile( File f ) throws FileNotFoundException, IOException 
    {
        FileInputStream in;
        OutputStream out;
      
        // Set up byte[]
        byte[] fileBytes = new byte[4096];
        
        in = new FileInputStream( f );
        out = sock.getOutputStream();
        
        int count;
        while((count = in.read(fileBytes)) > 0)
        {
            out.write(fileBytes, 0, count);
        }
        
        System.out.println("Sent " + f );
        out.close();
        in.close();
    }
    
    
    private void recvFile( String fName ) throws IOException
    {
        File f = new File("BCFiles/" + fName);
        InputStream in = sock.getInputStream();
        OutputStream out = new FileOutputStream( f );
        
        byte[] bytes = new byte[4096];
        
        int count;
        while((count = in.read(bytes)) > 0)
        {
            out.write(bytes, 0, count);
        }
        
        out.close();
        in.close();
        
    }
    
    private List<String> fileToString( String fName ) throws FileNotFoundException, IOException 
    {
        InputStream in = new FileInputStream(new File("BCFiles/" + fName));
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
//        StringBuilder out = new StringBuilder();
        List<String> strList = new ArrayList<String>();
        String line;
        while ((line = reader.readLine()) != null) {
            strList.add(line);
        }
        
        //Prints the string content read from input stream
        reader.close();

        
        return strList;
    }
    
    
    private String getFileNames()
    {
        File folder = new File("BCFiles");
        File[] listOfFiles = folder.listFiles();
        
        String fileList = "";

        for (int i = 0; i < listOfFiles.length; i++) {
          if (listOfFiles[i].isFile()) {
             fileList += listOfFiles[i].getName() + ":";
          } 
        }
        
        return fileList;
    }
    
    
}
