package netsecfinalserver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/**
 *
 * @author Max Kiniklis
 * 
 * This class should write the contents of the block chain to a file that gets
 * stored on the server. 
 * 
 * The block chain data is encrypted using a fresh AES key. That new key gets
 * encapsulated using the servers public key and stored in the same file as the 
 * encrypted block chain.
 */
public class BCWriter {
    private final String PUB_STRING = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCFYmWm9XY9lURihj6XQ9zERDQnikqHea3sIVDZskzJZJBY5PusJADddVTs2M5AslZuzS4sbSsnfb3gQj2AuDgYKPr40OP3QfYB1XQSj5RhZOOrKvZVtzpfBtZ8yovrid92KKfdgmOqkS5whRhaq+HsidSPWPVRqpoOxqSo0bjJXwIDAQAB";
    private PublicKey pubKey;
    private SecretKey aesKey;
    private String encData;
    private File bcData = new File( "BlockChainData.txt" );
    
    
    public BCWriter() throws NoSuchAlgorithmException, InvalidKeySpecException {
        // Generate AES Key
        this.aesKey = AES.genKey();
        
        // Establish the Public Key
        this.pubKey = RSA.strToPubKey( PUB_STRING );
    }
    
    /*
     * Encrypt the block data and then encapsulate the AES key and encrypted Data
     * using the servers public key and RSA. 
     * Pass them off to fileHandler().
    */
    public void saveChain( String data ) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, IOException
    {
        String aesKeyString, combinedData, encapKey;
        
        // Encrypt block data with AES
        this.encData = AES.encryptStr(data, this.aesKey);
        
        // Convert AES key to string
        aesKeyString = AES.keyToStr(this.aesKey);
        
        // Encapsulate AES Key using RSA
        encapKey = RSA.encryptStr(aesKeyString, this.pubKey);
        combinedData = encapKey + ":" + this.encData;
        
        // Save to file
        writeToFile( combinedData );
        
    }
    
    
    private void writeToFile( String data ) throws IOException
    {
        FileWriter fileWriter = new FileWriter( bcData );
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.print( data );
        printWriter.close();
    }
    
}
