package netsecfinalserver;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.Scanner;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.net.ssl.SSLServerSocketFactory;

/**
 *
 * @author Max Kiniklis
 */
public class NetSecFinalServer {

    private static PrivateKey privKey;
    private static PublicKey pubKey;
    private final static String PUB_STRING = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCFYmWm9XY9lURihj6XQ9zERDQnikqHea3sIVDZskzJZJBY5PusJADddVTs2M5AslZuzS4sbSsnfb3gQj2AuDgYKPr40OP3QfYB1XQSj5RhZOOrKvZVtzpfBtZ8yovrid92KKfdgmOqkS5whRhaq+HsidSPWPVRqpoOxqSo0bjJXwIDAQAB";
    private final static String PRIV_STRING = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAIViZab1dj2VRGKGPpdD3MRENCeKSod5rewhUNmyTMlkkFjk+6wkAN11VOzYzkCyVm7NLixtKyd9veBCPYC4OBgo+vjQ4/dB9gHVdBKPlGFk46sq9lW3Ol8G1nzKi+uJ33Yop92CY6qRLnCFGFqr4eyJ1I9Y9VGqmg7GpKjRuMlfAgMBAAECgYB70Q9ToOAV4o4Md1l6yUAR4M4ZNkJg6A8O9w46R00IVhW4rzcAYJt8+AHRqqArZmDdkhGeo+g/THYJyvOUnW1Th+BS1+jiuea446vH6yt2xQOsbm1wkazH5LJSb9c8NoocGmR1uSPNjt5ROa2Xe9yh5F6IDEadgzwGAWZHuM6XoQJBAMiA30t9HX41vPm6l5uPbUalKL/m0SuZhG9DdG3DwctOLKHxIRGCUZ8dWkUcQHwqvrQJpmbtTIyHcQCkuqjnACMCQQCqTabCM1mcH0IA67WIkMluCPonOB8AOAiWjhrAOBi4OpcwBqNFKSio5eDgEDqIGcHribeh3Y/Sxq5qGFUKlEeVAkB4IhcoYM+iBkqzCmS6NSaRVHXk/URIrlPWYoFJHyhnSvCdGx/oIMAM6QZkMDhG7UmUD9AfDYF3FdPUQd8BQl8nAkBs/rWzpNm9KeQ8Yp8UgFXqrqQrZaexpCsa8Gf+jeiEQDty6Uylfuoe7sE1pi1P3exWqxVz6zGZXL9T9661zqDhAkEAqJrVNfAg78TJTndOAhWQoQ+EKwcNbnd+J7+WQP1HsXlThAPzMYTn4UOwbDrEvUe1i6HuCAvbroZKrtoYRQO/wQ==";

    
    
    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, FileNotFoundException, ParseException {
        SSLServerSocketFactory sslFact;
        ServerSocket server;
        int PORT_NUM = 5000;
        /******** Server Startup Processes ********/
        
        // Turn the public/private keystrings into PublicKey PrivateKey Objects
        privKey = RSA.strToPrivKey( PRIV_STRING );
        pubKey = RSA.strToPubKey(PUB_STRING);
        
        // Create and load the Block Chain
        BlockChain bc = new BlockChain( privKey, pubKey );
        
        // Setup Scanner
        Scanner in = new Scanner(System.in);
        
        
        System.out.println("Server Start\n");
        // Set the keystore and keystore password.
        System.setProperty("javax.net.ssl.keyStore", "ps5keystore.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", "password");
        
        sslFact = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
        server = sslFact.createServerSocket(PORT_NUM);
        
        Socket sock = null;
                
        // Loop forever handing connections.
        while (true)
        {
            try {
                sock = server.accept();
            } catch (IOException e) {
                System.out.println("I/O error: " + e);
            }
            // new thread for a client
            new ClientThread( sock, bc ).start();
            
        }
        

        
    }
    
    
}
