package netsecfinalserver;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author Max Kiniklis
 */
public class RSA {
    
    // Convert B64 key string to public key
    public static PublicKey strToPubKey( String keyString ) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode( keyString ));
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(pubKeySpec);
    }
    
    public static PrivateKey strToPrivKey( String keyString ) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        PKCS8EncodedKeySpec privKeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode( keyString ));
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePrivate(privKeySpec);
    }
    
    
    // Encrypt a string and return a B64 version (String) of the cipher text
    public static String encryptStr( String plainText, PublicKey pubKey ) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
        // Encrypt combinedString with the public key
        Cipher rsaCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        rsaCipher.init(Cipher.ENCRYPT_MODE, pubKey);
        return Base64.getEncoder().encodeToString( rsaCipher.doFinal(plainText.getBytes()) );
    }
    
    // Encrypt a string and return a byte[] of the ciphertext
    public static byte[] encryptByte( String plainText, PublicKey pubKey ) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
        // Encrypt combinedString with the public key
        Cipher rsaCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        rsaCipher.init(Cipher.ENCRYPT_MODE, pubKey);
        return rsaCipher.doFinal(plainText.getBytes());
    }
    
    public static String decryptStr( String cipherText, PrivateKey privKey ) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
        byte[] cipherBytes = Base64.getDecoder().decode(cipherText.getBytes());
        
        // Decrypt RSA using the private key
        Cipher rsaCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        rsaCipher.init(Cipher.DECRYPT_MODE, privKey);
        
        String plainText = new String(rsaCipher.doFinal(cipherBytes));
        
        return plainText;
    }
    
    
    
}
