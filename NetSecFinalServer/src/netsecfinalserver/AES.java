package netsecfinalserver;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Max Kiniklis
 * 
 * This class should handle all encryption, decryption, and key generation using AES
 */
public class AES {
    
    // Generate a new SecretKey object for AES encryption
    public static SecretKey genKey() throws NoSuchAlgorithmException
    {
        // Generate AES Key
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(256); // for example
        SecretKey secretKey = keyGen.generateKey();
        
        return secretKey;
    }
    
    // Convert a b64 string into a SecretKeySpec object
    public static SecretKeySpec strToSecretKey( String strKey)
    {
        // Decode the AES key and generate SecretKey spec for decryption
        byte[] rawAESKey = Base64.getDecoder().decode( strKey );
        SecretKeySpec skeySpec = new SecretKeySpec(rawAESKey, "AES");
        
        return skeySpec;
    }
    
    public static String keyToStr( SecretKey key )
    {
        return Base64.getEncoder().encodeToString( key.getEncoded() );
    }
    
    // Encrypts string and returns b64 string
    public static String encryptStr( String plaintext, SecretKey key ) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException 
    {
        // Encrypt using AES
        Cipher aesCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        aesCipher.init(Cipher.ENCRYPT_MODE, key);
        
        // Encode cipher text to B64 String
        String cTextString = Base64.getEncoder().encodeToString( aesCipher.doFinal(plaintext.getBytes("UTF-8")) );
        
        return cTextString;
    }
    
    
    // Encrypts string and returns byte[]
    public static byte[] encryptByte( String plaintext, SecretKey key ) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException 
    {
        // Encrypt using AES
        Cipher aesCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        aesCipher.init(Cipher.ENCRYPT_MODE, key);
        
        // Encode cipher text to B64 String
        byte[] cTextString = aesCipher.doFinal(plaintext.getBytes("UTF-8"));
        
        return cTextString;
    }
    
    
    
    // Decrypt B64 strings using AES Key and return either a string or byte[]
    public static String decryptStr( String cipherText, String key ) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
        // Convert B64 key into SecretKeySpec
        SecretKeySpec aesKey = strToSecretKey( key );
        
        // Decrypt the ciphertext and return the plaintext
        Cipher aesCipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
        aesCipher.init(Cipher.DECRYPT_MODE, aesKey);
        return new String(aesCipher.doFinal(Base64.getDecoder().decode( cipherText )));
    }
    
    
   
    
    
}
