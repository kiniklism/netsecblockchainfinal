package netsecfinalserver;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Scanner;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author Max Kiniklis
 * 
 * This class should read in the contents of BlockChainData.txt, decrypt it,
 * and return the data to the Block Chain class for parsing.
 */
public class BCReader {
        private final static String PRIV_STRING = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAIViZab1dj2VRGKGPpdD3MRENCeKSod5rewhUNmyTMlkkFjk+6wkAN11VOzYzkCyVm7NLixtKyd9veBCPYC4OBgo+vjQ4/dB9gHVdBKPlGFk46sq9lW3Ol8G1nzKi+uJ33Yop92CY6qRLnCFGFqr4eyJ1I9Y9VGqmg7GpKjRuMlfAgMBAAECgYB70Q9ToOAV4o4Md1l6yUAR4M4ZNkJg6A8O9w46R00IVhW4rzcAYJt8+AHRqqArZmDdkhGeo+g/THYJyvOUnW1Th+BS1+jiuea446vH6yt2xQOsbm1wkazH5LJSb9c8NoocGmR1uSPNjt5ROa2Xe9yh5F6IDEadgzwGAWZHuM6XoQJBAMiA30t9HX41vPm6l5uPbUalKL/m0SuZhG9DdG3DwctOLKHxIRGCUZ8dWkUcQHwqvrQJpmbtTIyHcQCkuqjnACMCQQCqTabCM1mcH0IA67WIkMluCPonOB8AOAiWjhrAOBi4OpcwBqNFKSio5eDgEDqIGcHribeh3Y/Sxq5qGFUKlEeVAkB4IhcoYM+iBkqzCmS6NSaRVHXk/URIrlPWYoFJHyhnSvCdGx/oIMAM6QZkMDhG7UmUD9AfDYF3FdPUQd8BQl8nAkBs/rWzpNm9KeQ8Yp8UgFXqrqQrZaexpCsa8Gf+jeiEQDty6Uylfuoe7sE1pi1P3exWqxVz6zGZXL9T9661zqDhAkEAqJrVNfAg78TJTndOAhWQoQ+EKwcNbnd+J7+WQP1HsXlThAPzMYTn4UOwbDrEvUe1i6HuCAvbroZKrtoYRQO/wQ==";
        private PrivateKey privKey;
        private File bcData = new File("./BlockChainData.txt");
        
        public BCReader( PrivateKey prKey ) throws NoSuchAlgorithmException, InvalidKeySpecException 
        {
            // Establish private key
            this.privKey = prKey;
        }
        
        
        // Read in the BC Data file. Decapsulate the first line then use the
        // key to decrypt the data and return it to the block chain.
        public String readFile() throws FileNotFoundException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
        {
            // Create Scanner
            Scanner scan = new Scanner(bcData);
            
            // Get Data
            String fileData = scan.nextLine();
            String[] parsedData = fileData.split(":");
            String encapKey = parsedData[0];
            String encData = parsedData[1];
            
            // Retrieve the encapsulated key
            String aesKey = decapKey( encapKey );
            
            // Decrypt the data
            String plainTextData = decryptData( encData, aesKey );
            
            return plainTextData;
        }
        
        
        private String decapKey( String encapKey ) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException 
        {
            String aesKey = RSA.decryptStr( encapKey, this.privKey );
            return aesKey;
        }
        
        
        // Decrypt the Block Chain data using the given key
        private String decryptData( String encData, String aesKey ) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
        {
            String plainTextData = AES.decryptStr(encData, aesKey);
            return plainTextData;
        }
    
    
}
