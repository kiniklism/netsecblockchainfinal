package netsecfinalserver;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

/**
 *
 * @author Max Kiniklis
 * 
 * Code inspired by:
 * https://keyholesoftware.com/2018/04/10/blockchain-with-java/
 */
public class Block {
    
    public String timestamp;
    private int index;
    public String currHash;    //Stored as HEX
    public String prevHash;    //Stored as HEX
    public String data;
    public String docName;
    public String transactionType;
    public String lastEditor;
    public String signature;   //Stored as B64   
    
    
    /******** Basic Block Functions ********/
    
    public void newBlock( int index, String data, String docName, String transType, String editor, String prevHash, PrivateKey sigKey ) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException
    {
        this.data = data;
        this.docName = docName;
        this.transactionType = transType;
        this.lastEditor = editor;
        this.prevHash = prevHash;
        this.index = index;
        
        // Create Timestamp
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        this.timestamp = ts.toString();
        
        // Hash the block
        this.currHash = hashBlock();
        
        // Sign the block
        sign( sigKey );
    }
    
    // This function will take in an argument and generate a hash of that arg and return it.
    private String hashBlock() throws NoSuchAlgorithmException
    {
        
        String dataToHash = this.timestamp + this.index + this.prevHash + this.docName + this.transactionType + this.data + this.lastEditor;
   
        MessageDigest md = MessageDigest.getInstance("SHA-256");  
        
        return toHexString( md.digest( dataToHash.getBytes( StandardCharsets.UTF_8 )));
    }
    
    
    // Convert MessageDigest to Hex
    private String toHexString(byte[] hash) 
    { 
        // Convert byte array into signum representation  
        BigInteger number = new BigInteger(1, hash);  
  
        // Convert message digest into hex value  
        StringBuilder hexString = new StringBuilder(number.toString(16));  
  
        // Pad with leading zeros 
        while (hexString.length() < 32)  
        {  
            hexString.insert(0, '0');  
        }  
  
        return hexString.toString();  
    } 
    
    
    // Sign the whole block after creation
    private void sign( PrivateKey key ) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException
    {
        String block = this.index + this.timestamp + this.currHash + this.prevHash + this.data + this.docName + this.transactionType + this.lastEditor;
        
        // Sign the whole block
        Signature sign = Signature.getInstance("SHA256withRSA");
        sign.initSign( key );
        
        sign.update( block.getBytes() );
        
        byte[] signature = sign.sign();
        this.signature = Base64.getEncoder().encodeToString( signature );
    }
    
    
    
    
    /******** Save Block Functions ********/    
    // Condense all of the block data to save it on the server
    public String condenseBlock()
    {
        String seperator = "|";
        String blockData = this.index + seperator + this.timestamp + seperator + this.currHash + 
                seperator + this.prevHash + seperator + this.data + seperator + this.docName + 
                seperator + this.transactionType + seperator + this.lastEditor + seperator + this.signature + ";";
        
        return blockData;
    }

    
    
    /******** Load Block Functions ********/
    public void loadBlock( int index, String ts, String blockHash, String prevHash, String data, String docName, String transType, String editor, String sig ) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException, ParseException
    {
        this.index = index;
        this.data = data;
        this.docName = docName;
        this.transactionType = transType;
        this.lastEditor = editor;
        this.currHash = blockHash;
        this.prevHash = prevHash;
        this.timestamp  = ts;
        this.signature = sig;
        
        
    }
    
    
    // Validates the signature of the block
    public boolean verifySignature( PublicKey pubKey ) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException
    {
        // Data to be verified
        String block = this.index + this.timestamp + this.currHash + this.prevHash + this.data + this.docName + this.transactionType + this.lastEditor;
        
        // Create Signature Object
        Signature sign = Signature.getInstance("SHA256withRSA");
        sign.initVerify( pubKey );
        
        // Update data to be verified
        sign.update( block.getBytes() );
   
        
        byte[] blockSig = Base64.getDecoder().decode( this.signature );
        
        return sign.verify( blockSig );
    }
    
    // Validate the hash value of the block
    public boolean validateHash()
    {
        return false;
    }    
    
    
    /******** SET & GET ********/
    public void setPrevHash(String prevHash) {
        this.prevHash = prevHash;
    }

    public String getCurrHash() {
        return currHash;
    }

    public String getPrevHash() {
        return prevHash;
    }
    
}